package nl.utwente.di.temp;

import java.util.Map;

public class Converter {

    Map<String, Double> myMap;

    public Converter() {

    }

    public double getFahrenheit(double isbn){
        return ((isbn * 1.8) + 32);
    }
}
